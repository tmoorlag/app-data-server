package controllers

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)


var (
	loginRequestJSON = `{"username":"jonsnow","password":"124"}`
)

func TestLogin(t *testing.T) {
	// Setup
	e := echo.New()

	data := url.Values{}
  data.Set("username", "foo")
  data.Set("password", "bar")

	req := httptest.NewRequest(http.MethodPost, "/login", strings.NewReader(data.Encode()))
	// req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()

	c := e.NewContext(req, rec)

	// Assertions
	if assert.Error(t, Login(c)) {
		resp := rec.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	fmt.Println(resp.StatusCode)
	fmt.Println(string(body))
		assert.Equal(t, http.StatusBadGateway, rec.Code)
	}
}
